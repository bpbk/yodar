//
//  ViewController.swift
//  Yodar
//
//  Created by Brandon Phillips on 6/7/15.
//  Copyright (c) 2015 Full Metal Workshop. All rights reserved.
//

import UIKit
import MapKit


class yoNote{
    var noteId : String?
    var noteFromName : String?
    var noteTo : String?
    var noteToName : String?
    var noteCoord : CLLocationCoordinate2D?
    var noteTitle : String?
    var noteMsg : String?
}

class CustomAnnotation: MKPointAnnotation {
    var pinId: String!
    var pinType: Int! // 0: MY PINS 1: PINS I DROPPED
    var pinImage: String!
}

class ViewController: UIViewController, MKMapViewDelegate {
    
    var appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
    var myLocation: CLLocationCoordinate2D!
    var pinLocation: CLLocationCoordinate2D?
    @IBOutlet weak var searchText: UITextField!
    var matchingItems: [MKMapItem] = [MKMapItem]()
    var fNote :yoNote?
    
    @IBOutlet weak var mapView: MKMapView!
    
    func updateMap(){
        myLocation = appDelegate.myCoords
        let region = MKCoordinateRegionMakeWithDistance(
            myLocation!, 1, 1)
        print("my coords \(appDelegate.myCoords)")
        mapView.setRegion(region, animated: true)
        mapView.setCenterCoordinate(myLocation, animated: true)
        
        performSearch()
    }
    
    func performSearch() {
        matchingItems.removeAll()
        let request = MKLocalSearchRequest()
        request.naturalLanguageQuery = "frozen yogurt"
        request.region = MKCoordinateRegionMakeWithDistance(
            myLocation!, 1, 1)
        print("region \(request.region)")
        
        let search = MKLocalSearch(request: request)

        search.startWithCompletionHandler{ (response, error) in
            
            if error != nil {
                print("Error occured in search: \(error!.localizedDescription)")
            } else if response!.mapItems.count == 0 {
                print("No matches found")
            } else {
                print("Matches found")
                
                var zoomRect : MKMapRect = MKMapRectNull
                
                for item in response!.mapItems {
                    //println("Name = \(item.name)")
                    //println("Phone = \(item.phoneNumber)")
                    
                    self.matchingItems.append(item as MKMapItem)
                    //print("Matching items = \(self.matchingItems.count)")
                    
                    let annotation = CustomAnnotation()
                    annotation.coordinate = item.placemark.coordinate
                    annotation.title = item.name
                    annotation.pinImage = "sb"
                    self.mapView.addAnnotation(annotation)
                    //print(annotation.title)
                    
                    let annotationPoint = MKMapPointForCoordinate(annotation.coordinate);
                    let pointRect = MKMapRectMake(annotationPoint.x, annotationPoint.y, 1, 1);
                    zoomRect = MKMapRectUnion(zoomRect, pointRect);
                    print("zoom rect \(zoomRect)")
                }
                
               self.mapView.setVisibleMapRect(zoomRect, animated: true)

            }
        }
    }
    
    func mapView(mapView: MKMapView, viewForAnnotation annotation: MKAnnotation) -> MKAnnotationView? {
        if !(annotation is CustomAnnotation) {
            return nil
        }
        
        let reuseId = "test"
        
        var anView = mapView.dequeueReusableAnnotationViewWithIdentifier(reuseId)
        if anView == nil {
            anView = MKAnnotationView(annotation: annotation, reuseIdentifier: reuseId)
            anView!.canShowCallout = true
        }
        else {
            anView!.annotation = annotation
        }
        
        //Set annotation-specific properties **AFTER**
        //the view is dequeued or created...
        
        let cpa = annotation as! CustomAnnotation
        anView!.image = UIImage(named:cpa.pinImage)
        
        return anView
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "updateMap", name: "locationUpdated", object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "performSearch", name: "StatusChanged", object: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

}

