//
//  AppDelegate.swift
//  Yodar
//
//  Created by Brandon Phillips on 6/7/15.
//  Copyright (c) 2015 Full Metal Workshop. All rights reserved.
//

import UIKit
import Foundation
import CoreData
import CoreLocation

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, CLLocationManagerDelegate {

    var window: UIWindow?
    var locationManager = CLLocationManager()
    var gotLocation : Bool?
    var locError : Bool?
    var locationStatus : String?
    var myCoords :CLLocationCoordinate2D?

    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        // Override point for customization after application launch.
        startLocationManager()
        return true
    }
    
    func startLocationManager(){
        locError = false
        gotLocation = false
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestWhenInUseAuthorization()
        locationManager.startUpdatingHeading()
    }
    
    func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if (gotLocation == false) {
            gotLocation = true
            let locationArray = locations as NSArray
            let locationObj = locationArray.lastObject as! CLLocation
            myCoords = locationObj.coordinate
            NSNotificationCenter.defaultCenter().postNotificationName("locationUpdated", object: nil)
        }
    }
    
    func locationManager(manager: CLLocationManager,
        didChangeAuthorizationStatus status: CLAuthorizationStatus) {
            var locationAllowed = false
            
            switch status {
            case CLAuthorizationStatus.Restricted:
                locationStatus = "Restricted Access"
            case CLAuthorizationStatus.Denied:
                locationStatus = "User Denied Access"
            case CLAuthorizationStatus.NotDetermined:
                locationStatus = "Status not determined"
            default:
                locationStatus = "Allowed Access"
                locationAllowed = true
            }
            //print(locationStatus!)
            
            if (locationAllowed == true) {
                locationManager.startUpdatingLocation()
                //NSNotificationCenter.defaultCenter().postNotificationName("StatusChanged", object: nil)
                
            } else {
                // USER DENIED ACCESS
            }
            
    }
    
    func locationManager(manager: CLLocationManager, didFailWithError error: NSError) {
        locationManager.stopUpdatingLocation()
            if (locError == false) {
                locError = true
                print(error)
            }
    }

    func applicationWillResignActive(application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


}

